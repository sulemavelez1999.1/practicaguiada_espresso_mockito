package facci.pm.dayoftheweek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button buttonCheck;
    EditText editText;

    //OnCreate is one of the methods of the life cycle of an activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Define the layout for this class
        setContentView(R.layout.activity_main);

        //Using findViewById i can make a relation with a view
        editText = (EditText)findViewById(R.id.editText2);
        buttonCheck = (Button) findViewById(R.id.button);

        //Set a listener for buttonCheck
        //This will be listening everytime that the button is pressed
        buttonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // If editText is empty, then show a error message to the user
                if (editText.getText().toString().trim().equalsIgnoreCase(""))
                {
                    editText.setError( getResources().getString( R.string.campo_blanco));
                }
                else {

                    //With an explicit intent i can call an activity and send values using
                    //putExtra method
                    Intent intent = new Intent(MainActivity.this, CheckActivity.class);
                    intent.putExtra("numeroSemana", editText.getText().toString());

                    //startActivity start the other activity, is very important
                    //call the finish method to close the currente activity
                    startActivity(intent);
                    finish();
                }
            }
        });

    }
}