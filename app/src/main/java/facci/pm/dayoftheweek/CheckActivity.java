package facci.pm.dayoftheweek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

public class CheckActivity extends AppCompatActivity {
    //Create variable for views
    TextView textView;
    Integer parametro;
    Button buttonTryAgain;

    //OnCreate is one of the methods of the life cycle of an activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Define the layout for this class
        setContentView(R.layout.activity_check);

        //Using findViewById set a view from the activity
        textView = (TextView)findViewById(R.id.textView2);
        buttonTryAgain = (Button)findViewById(R.id.ButtonTryAgain);

        //Using getIntent and getExtras i receive the parameters and values comming from the past activty
        parametro = Integer.parseInt( getIntent().getExtras().getString("numeroSemana"));

        //Create a instance of Calendar class to get the date
        Calendar calendario = Calendar.getInstance();

        Log.e("Week of the current year", String.valueOf(calendario.get(Calendar.WEEK_OF_YEAR)));

        //Create a instance of MediaPlayer to manage audio in this activity
        MediaPlayer player;

        //If the currente number of the week is equal to the parameter then win the game
        //I use the constant value for the number of the week in the Calendar class
        if (  calendario.get(Calendar.WEEK_OF_YEAR)  ==  parametro )
        {
            //If value is ok then assign "Rigth" to the textView, apply a style and play the win sound
            textView.setText(getResources().getString( R.string.mensaje_correcto));
            textView.setText("GREAT!!");
            //Assign the sound
            player = MediaPlayer.create(this, R.raw.tada);
            //Play the sound
            player.start();


        }
        else
        {
            //If value is wrong then assign the fail text to the textView, apply a style and play the fail sound
            textView.setText(getResources().getString( R.string.mensaje_error));
            textView.setText("FAIL =(");
            //Assign the sound
            player = MediaPlayer.create(this, R.raw.bite);
            //Play the sound
            player.start();
        }


        //Set a listener for buttonCheck
        //This will be listening everytime that the button is pressed
        //This method call the main activty (first activity) and close the currente activity to try again
        buttonTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intento = new Intent(CheckActivity.this, MainActivity.class);
                startActivity(intento);
                finish();
            }
        });

    }
}